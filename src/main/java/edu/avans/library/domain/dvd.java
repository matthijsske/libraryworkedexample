package edu.avans.library.domain;

/**
 * Created by Matthijsske on 7-9-2015.
 */
public class Dvd extends ItemImpl {
    private String title;
    private String publisher;

    public Dvd(String title, String publisher, int year, int loanperiod, int code) {
        this.title = title;
        this.publisher = publisher;
        setYear(year);
        setLoanPeriod(loanperiod);
        setCode(code);
    }

    public String getTitle() {
        return title;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    @Override
    public boolean equals(Object o) {
        boolean equal = false;

        if (o == this) {
            equal = true;
        }
        else {
            if (o instanceof Dvd) {
                Dvd d = (Dvd) o;

                equal = this.getCode() == d.getCode();
            }
        }
        return equal;
    }

    @Override
    public int hashCode() {
        return getCode();
    }
}
