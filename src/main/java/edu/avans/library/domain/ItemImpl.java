package edu.avans.library.domain;

/**
 * Created by Matthijsske on 7-9-2015.
 */
public class ItemImpl implements Item {
    private int year;
    private int loanPeriod;
    private int code;
    private String genre;

    @Override
    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public void setLoanPeriod(int loanPeriod) {
        this.loanPeriod = loanPeriod;
    }

    @Override
    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public void setGenre(String genre) {
        this.genre = genre;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public int getYear() {
        return year;
    }

    @Override
    public int getLoanPeriod() {
        return loanPeriod;
    }

    @Override
    public String getGenre() {
        return genre;
    }
}
