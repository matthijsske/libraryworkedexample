package edu.avans.library.domain;

/**
 * Created by Matthijsske on 7-9-2015.
 */
public interface Item {
    int getYear();
    int getLoanPeriod();
    int getCode();
    String getGenre();
    void setYear(int year);
    void setLoanPeriod(int loanPeriod);
    void setCode(int code);
    void setGenre(String genre);
}
